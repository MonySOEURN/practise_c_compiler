#include<stdio.h>
#include<stdlib.h>
#include "semantic.h"
#include "symtab.h"

void set_type(char *name, int st_type, int inf_type){
	/* lookup entry */
	list_t *l = lookup(name);

	/* set "main" type */
	l->st_type = st_type;

	/* if array, pointer or function */
	if(inf_type != UNDEF){
		l->inf_type = inf_type;
	}	
}

int get_type(char *name){
    /* Look up entry */
    list_t *l = lookup(name);

    /* if "simple" type */
    if(l->st_type == INT_TYPE || l->st_type == REAL_TYPE || l->st_type == CHAR_TYPE){
        return l->st_type;
    }
    /* if array, pointer or function */
    else {
        return l->inf_type;
    }    
}

int get_result_type(int type_1, int type_2, int op_type){
    switch(op_type){
        /* ---------------------------------------------------------- */
        case NONE: /* type compatibility only, '1': compatible */
            // first type INT
            if(type_1 == INT_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return 1;
                }
                else{
                type_error(type_1, type_2, op_type);
                }
            }
            // first type REAL
            else if(type_1 == REAL_TYPE){
                // second type INT, REAL or CHAR
                if(type_2 == INT_TYPE || type_2 == REAL_TYPE || type_2 == CHAR_TYPE){
                    return 1;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            // first type CHAR
            else if(type_1 == CHAR_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return 1;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            break;
        /* ---------------------------------------------------------- */
        case ARITHM_OP: /* arithmetic operator */
            // first type INT
            if(type_1 == INT_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return INT_TYPE;
                }
                // second type REAL
                else if(type_2 == REAL_TYPE){
                    return REAL_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            // first type REAL
            else if(type_1 == REAL_TYPE){
                // second type INT, REAL or CHAR
                if(type_2 == INT_TYPE || type_2 == REAL_TYPE || type_2 == CHAR_TYPE){
                    return REAL_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            // first type CHAR
            else if(type_1 == CHAR_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return CHAR_TYPE;
                }
                // second type REAL
                else if(type_2 == REAL_TYPE){
                    return REAL_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            else{
                type_error(type_1, type_2, op_type);
            }
            break;
        /* ---------------------------------------------------------- */
        case INCR_OP: /* special case of INCR */
            // type INT
            if(type_1 == INT_TYPE){
                return INT_TYPE;
            }
            // type REAL
            else if(type_1 == REAL_TYPE){
                return REAL_TYPE;
            }
            // type CHAR
            else if(type_1 == CHAR_TYPE){
                return CHAR_TYPE;
            }
            else{
                type_error(type_1, type_2, op_type);
            }
            break;
        /* ---------------------------------------------------------- */
        case BOOL_OP: /* Boolean operator */
            // first type INT
            if(type_1 == INT_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return INT_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            // first type CHAR
            else if(type_1 == CHAR_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return CHAR_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            else{
                type_error(type_1, type_2, op_type);
            }
            break;
        case NOT_OP: /* special case of NOTOP */
            // type INT
            if(type_1 == INT_TYPE){
                return INT_TYPE;
            }
            // type CHAR
            else if(type_1 == CHAR_TYPE){
                return INT_TYPE;
            }
            else{
                type_error(type_1, type_2, op_type);
            }
            break;
        /* ---------------------------------------------------------- */
        case REL_OP: /* Relational operator */
            // first type INT
            if(type_1 == INT_TYPE){
                // second type INT, REAL or CHAR
                if(type_2 == INT_TYPE || type_2 == REAL_TYPE || type_2 == CHAR_TYPE){
                    return INT_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            else if(type_1 == REAL_TYPE){
                // second type INT, REAL or CHAR
                if(type_2 == INT_TYPE || type_2 == REAL_TYPE || type_2 == CHAR_TYPE){
                    return INT_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            // first type CHAR
            else if(type_1 == CHAR_TYPE){
                // second type INT, REAL or CHAR
                if(type_2 == INT_TYPE || type_2 == REAL_TYPE || type_2 == CHAR_TYPE){
                        return INT_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            else{
                type_error(type_1, type_2, op_type);
            }
            break;
        /* ---------------------------------------------------------- */
        case EQU_OP: /* Equality operator */
            // first type INT
            if(type_1 == INT_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return INT_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            else if(type_1 == REAL_TYPE){
                // second type REAL
                if(type_2 == REAL_TYPE){
                    return INT_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            // first type CHAR
            else if(type_1 == CHAR_TYPE){
                // second type INT or CHAR
                if(type_2 == INT_TYPE || type_2 == CHAR_TYPE){
                    return INT_TYPE;
                }
                else{
                    type_error(type_1, type_2, op_type);
                }
            }
            else{
                type_error(type_1, type_2, op_type);
            }
            break;
        /* ---------------------------------------------------------- */
        default: /* wrong choice case */
            fprintf(stderr, "Error in operator selection!\n");
            exit(1);
    }
}

void type_error(int type_1, int type_2, int op_type){ 
	fprintf(stderr, "Type conflict between %d and %d using op type %d\n",
         type_1, type_2, op_type);
	exit(1);
}

Param def_param(int par_type, char *param_name, int passing){
    Param param; /* Parameter struct */

    /* set the information */
    param.par_type = par_type;
    strcpy(param.param_name, param_name);
    param.passing = passing;

    /* return the structure */
    return param;
}

int func_declare(char *name, int ret_type, int num_of_pars, Param *parameters){
    /* lookup entry */
    list_t *l = lookup(name);

    /* if type is not defined yet */
    if(l->st_type != UNDEF){
        /* entry is of function type */
        l->st_type = FUNCTION_TYPE;

        /* return type is ret_type */
        l->inf_type = ret_type;

        /* parameter stuff */
        l->num_of_pars = num_of_pars;
        l->parameters = parameters;

        return 0; /* success */
    }
    /* already declared error */
    else{
        fprintf(stderr, "Function %s already declared!\n", name);
        exit(1);
    }
}

/* check parameter of actual and declaration function */
int func_param_check(char *name, int num_of_pars, Param *parameters){
    int i, type_1, type_2;

    /* lookup entry */
    list_t *l = lookup(name);

    /* check number of parameters */
    if(l->num_of_pars != num_of_pars){
        fprintf(stderr, "Function call of %s has wrong num of parameters!\n", name);
        exit(1);
    }

    /* check if parameters are compatible */
    for(i = 0; i < num_of_pars; i++){
        /* type of parameter in function declaration */
        type_1 = l->parameters[i].par_type; 

        /* type of parameter in function call*/
        type_2 = parameters[i].par_type; 

        /* check compatibility for function call */
        get_result_type(type_1, type_2, NONE);
        /* error occurs automatically in the function */
    }

    return 0; /* success */
}